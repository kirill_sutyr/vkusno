//
//  CameraViewController.swift
//  Recipe
//
//  Created by Dmytro Nour on 12/18/19.
//  Copyright © 2019 Dmytro Nour. All rights reserved.
//

import UIKit
import AVKit
import Vision

class CameraViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    var productLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        productLabel = UILabel()
        productLabel.text = "Product"
        productLabel.sizeToFit()
        productLabel.textColor = .white
        
        
        
        
        
        let captureSession = AVCaptureSession()
        
        guard let captureDevice = AVCaptureDevice.default(for: .video) else { return }
        guard let input = try? AVCaptureDeviceInput(device: captureDevice) else { return }
        captureSession.addInput(input)
        
        captureSession.startRunning()
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        view.layer.addSublayer(previewLayer)
        previewLayer.frame = view.frame
        view.addSubview(productLabel)
        productLabel.center = CGPoint(x: 200, y: 700)
        
        productLabel.adjustsFontSizeToFitWidth = true
        
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue" ))
        captureSession.addOutput(dataOutput)
//        let request =
    }
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return}
        guard let model = try? VNCoreMLModel(for: Resnet50().model) else { return }
        let request = VNCoreMLRequest(model: model) { (finish, err) in

            guard let results = finish.results as? [VNClassificationObservation] else {return}
            guard let firstObservation = results.first else { return }
            DispatchQueue.main.async {
                self.productLabel.text = firstObservation.identifier
            }
            
            print(firstObservation.identifier, firstObservation.confidence)
        }
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
        
    }
//    func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
//        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return}
//        guard let model = try? VNCoreMLModel(for: Resnet50().model) else { return }
//        let request = VNCoreMLRequest(model: model) { (finish, err) in
//            print(finish.results)
//            guard let results = finish.results as? [VNClassificationObservation] else {return}
//            guard let firstObservation = results.first else { return }
//            print(firstObservation.identifier, firstObservation.confidence)
//        }
//        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
//
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
