//
//  ViewController.swift
//  Recipe
//
//  Created by Dmytro Nour on 12/17/19.
//  Copyright © 2019 Dmytro Nour. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var titleNextPage: String?
    var dataSourseToSend: [Recipe]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    func showFirst() {
        dataSourseToSend = allRecipies.shared.recipies.filter({ (recipe) -> Bool in
            if recipe.category == "Первые блюда" {
                return true
            } else { return false}
        })
    }
    func showBakery() {
        dataSourseToSend = allRecipies.shared.recipies.filter({ (recipe) -> Bool in
            if recipe.category == "Выпечка" {
                return true
            } else { return false}
        })
    }
    func showMain() {
        dataSourseToSend = allRecipies.shared.recipies.filter({ (recipe) -> Bool in
            if recipe.category == "Основные блюда" {
                return true
            } else { return false}
        })
    }
    func showDesert() {
        dataSourseToSend = allRecipies.shared.recipies.filter({ (recipe) -> Bool in
            if recipe.category == "Десерты" {
                return true
            } else { return false}
        })
    }
    func showDrink() {
        dataSourseToSend = allRecipies.shared.recipies.filter({ (recipe) -> Bool in
            if recipe.category == "Напитки" {
                return true
            } else { return false}
        })
    }
    func showZakus() {
        dataSourseToSend = allRecipies.shared.recipies.filter({ (recipe) -> Bool in
            if recipe.category == "Закуски" {
                return true
            } else { return false}
        })
    }
    func showAll() {
        dataSourseToSend = allRecipies.shared.recipies
    }
    
    @IBAction func allRecipiesToched(_ sender: Any) {
        showAll()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    @IBAction func findTouched(_ sender: Any) {
        showCheap()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    
    @IBAction func ARtapped(_ sender: Any) {
        
        showSearch()
        
        
        // performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    @IBAction func firstTapped(_ sender: Any) {
        showFirst()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    @IBAction func mainTapped(_ sender: Any) {
        showMain()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    @IBAction func ZakusTapped(_ sender: Any) {
        showZakus()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    @IBAction func desertTapped(_ sender: Any) {
        showDesert()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    @IBAction func bakeryTapped(_ sender: Any) {
        showBakery()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    
    @IBAction func drinksTapped(_ sender: Any) {
        showDrink()
        performSegue(withIdentifier: "recepiesTable", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? RecipiesTableViewController {
            viewController.dataSourse = dataSourseToSend
            
        }
        
        titleNextPage = ""
        dataSourseToSend?.removeAll()
    }
    
    func showSearch() {
        performSegue(withIdentifier: "cameraSegue", sender: self)
    }
    
    func showCheap() {
        dataSourseToSend = allRecipies.shared.recipies.sorted { (left, right) -> Bool in
            if left.cost < right.cost {
                return true
            } else { return false }
        }
        dataSourseToSend = dataSourseToSend?.dropLast(13)
    }
}

