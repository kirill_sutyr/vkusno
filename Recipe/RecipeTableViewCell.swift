//
//  RecipeTableViewCell.swift
//  Recipe
//
//  Created by Dmytro Nour on 12/18/19.
//  Copyright © 2019 Dmytro Nour. All rights reserved.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    func configure(with recipe: Recipe) {
        imageCell.image = recipe.image
        name.text = recipe.name
        var temp = ""
        for i in recipe.ingridients {
            temp += i
            temp += ", "
        }
        ingridientList.text = temp
        timeString.text = recipe.time
        costString.text = "\(recipe.cost) грн."
    }
    @IBOutlet weak var imageCell: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ingridientList: UITextView!
    @IBOutlet weak var timeString: UILabel!
    @IBOutlet weak var costString: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
