//
//  SingleRecipeViewController.swift
//  Recipe
//
//  Created by Dmytro Nour on 12/18/19.
//  Copyright © 2019 Dmytro Nour. All rights reserved.
//

import UIKit

class SingleRecipeViewController: UIViewController {

    var recipe: Recipe?
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == .right {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        imageCell.image = recipe?.image
        textView.text = recipe?.fullRecipe
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
