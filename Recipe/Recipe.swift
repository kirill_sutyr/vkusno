//
//  Recipe.swift
//  Recipe
//
//  Created by Dmytro Nour on 12/17/19.
//  Copyright © 2019 Dmytro Nour. All rights reserved.
//

import Foundation
import UIKit

class  Recipe {
    let image: UIImage
    let name: String
    let ingridients: [String]
    let time: String
    let cost: Int
    let category: String
    let fullRecipe: String
    
    init(image: UIImage, name: String, ingridients: [String], time: String,cost: Int, category: String, fullRecipe: String) {
        self.image = image
        self.cost = cost
        self.ingridients = ingridients
        self.name = name
        self.time = time
        self.category = category
        self.fullRecipe = fullRecipe
    }
}

class allRecipies {
    
    static var shared = allRecipies()
    
    
    var recipies = [Recipe]()
    init() {
        recipies.append(Recipe(image: #imageLiteral(resourceName: "1") , name: "Суп-пюре из цветной капусты с пармезаном", ingridients: ["Чеснок","Мускатный орех","Сыр пармезан","Лук репчатый","Цветная капуста","Морская соль","Оливковое масло"], time: "15 минут", cost: 70, category: "Первые блюда", fullRecipe: """
            В кастрюле с толстым дном пассеровать на оливковом масле измельченные лук и чеснок.
            Цветную капусту разобрать на соцветия и отварить в течение 5–7 минут.
            Отваренную капусту переложить в кастрюлю с луком, добавить немного бульона от капусты и тушить под крышкой до готовности.
            Посолить и добавить немного мускатного ореха.
            Перетереть через сито или перемолоть в блендере.
            Разлить по огнеупорным тарелкам, посыпать тертым пармезаном, по желанию, гренками (чесночными) и поставить в духовку на 5 минут (180 градусов).
            Чтобы добиться сливочного вкуса можно добавить 2 столовые ложки нежирных сливок.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "2") , name: "Постный борщ со щавелем", ingridients: ["Свекла", "Луковица", "Морковь", "Соль", "Сахар", "Щавель", "Растительное масло", "Картофель", "Специи", "Томат"], time: "1 ч. 0 мин", cost: 60, category: "Первые блюда", fullRecipe: """
            1. Вот такой довольно скромный и доступный набор ингредиентов вам потребуется, если вы решите приготовить постный борща со щавелем на вашей кухне.
        2. На дно чаши мультиварки налейте немного масла, включите режим "Жарка". Очистите и измельчите лук. Выложите его в чашу и обжарьте пару минут.
        3. Очистите и натрите на терке морковь. Добавьте её к луку и жарьте еще минут 5, помешивая.
        4. Свеклу также натрите на терке и обжаривайте в чаше 6-7 минут. Также вы можете использовать чеснок, сладкий и острый перцы.
        5. Добавьте томат или пасту (предварительно разведите ее небольшом количестве воды). Подсолите немного, добавьте ложку сахара и щепотку перца.
        6. Включите режим "Суп" и дайте содержимому чаши закипеть. За это время очистите картофель, нарежьте его небольшими кубиками и добавьте в чашу.
        7. Выложите измельченный щавель. Он может быть как свежим, так и замороженным или консервированным, например.
        8. Влейте воду или овощной бульон. Добавьте по вкусу соль и любимые специи. Закройте крышку и оставьте постный борщ с щавелем в домашних условиях минут на 40.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "3") , name: "Рассольник с курицей и рисом", ingridients: ["Перец чили", "Соленые огурцы", "Морковь", "Картофель", "Соль", "Растительное масло", "Перец", "Лук", "Чеснок", "Курица", "Рис", "Сметана"], time: "1 ч. 0 мин", cost: 170, category: "Первые блюда", fullRecipe: """
            Рис промываем и замачиваем в холодной воде на 1 час минимум.
        В кастрюлю положить промытые куриные бедра, залить водой и довести до кипения, пену снять. Затем огонь убавить. Варить накрыв крышкой.
        Лук почистить и нарезать полукольцами. Морковь помыть, почистить, натереть на терке. Овощи потушить на растительном масле, добавить немного чесноку, соль и перец по вкусу.
        Из бульона, который варился примерно час, достапть мясо и порезать его на кусочки. А в бульон положить очищенную и нарезанную кубиками картошку, рис, перчик чили и варить до готовности картошки и риса 20-25 минут. За 5 минут добавить тушеные овощи.
        Соленые огурцы мелко порубить. Оставить один стакан рассола. В конце варки супа в бульон добавить огурцы, рассол и нарезанное кусочками мясо.
        Готовый рассольник разлить по тарелкам, заправить сметаной. Готово! Приятного аппетита!
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "4") , name: "Запеканка из цветной капусты с сыром", ingridients: ["Сыр твердый", "Вода", "Соль", "Яйца куриные", "Цветная капуста", "Укроп", "Молоко"], time: "1 ч. 0 мин", cost: 115, category: "Основные блюда", fullRecipe: """
            1) Вымытую и очищенную от листьев капусту разобрать на соцветия, удалив грубые ножки.
        2) Отварить капусту в соленой воде около 7 минут после закипания. На два литра воды я положила 2 ч. л. соли.
        3) По прошествии времени откинуть цветную капусту на дуршлаг, чтобы стекла лишняя вода.
        4) Отваренные соцветия разделить на более мелкие, чтобы их было удобнее запекать.
        5) Приготовить яичную смесь. Для этого разбить в миску яйца, взбить их, добавить молоко, положить 0,5 ч. л. соли и еще раз взбить.
        6) В форму для запекания выложить соцветия. Я использовала силиконовую форму диаметром 22 сантиметра. Смазывать силиконовую форму растительным маслом не нужно. Если вы используете обычную форму, то смажьте ее дно растительным маслом и обсыпьте его панировочными сухарями для предотвращения пригорания.
        7) Влить в форму яичную смесь, равномерно распределив ее по всей форме.
        8) Поставить форму в разогретую духовку и оставить ее запекаться на 30 минут. Выпекать при температуре 200 градусов.
        Духовки у всех пекут по-разному, поэтому периодически проверяем готовность запеканки зубочисткой. Запеканка должна затвердеть.
        9) Пока запеканка готовится в духовке, натираем сыр. Можно взять любой твёрдый сыр, который вам нравится. Я использовала сыр «Гауда».
        10) Достаем запеканку из духовки и равномерно посыпаем ее тертым сыром, после чего ставим ее обратно в духовку и оставляем на 10 минут, запекая при той же температуре.
        11) Нарезаем свежую зелень, чтобы украсить готовую запеканку. Я взяла укроп, но подойдет и любая другая зелень, которая вам нравится.
        12) Когда сыр расплавится, достаем запеканку из духовки и посыпаем ее зеленью.
        Приятного аппетита!
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "5") , name: "Содовая курочка", ingridients: ["Специи", "Сода", "Куриное филе", "Соль", "Мука", "Кунжут", "Крахмал картофельный"], time: "40 минут", cost: 100, category: "Основные блюда", fullRecipe: """
            Нарезаем куриное филе (у меня 600 г), солим и посыпаем содой: так, чтобы немного соды попало на каждый кусочек филе (1-1,5 ч.л. соды на 600 г филе). Добавляем немного лимонного сока, перемешиваем и наблюдаем бурное вспенивание. Посыпаем курицу картофельным крахмалом так, чтобы им был покрыт каждый кусочек филе. Оставляем на 15 минут. Спустя 15 минут обваливаем каждый кусочек филе в муке и обжариваем на любом масле. Во время обжарки кусочки не должны соприкасаться друг с другом. В муку можно ничего не добавлять, а можно добавить ваши любимые специи (у меня в этот раз сушёный базилик и кунжут).
        """
        ))
        
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "6") , name: "Спагетти Маринара с анчоусами", ingridients: ["Перец черный молотый", "Чеснок", "Помидоры в собственном соку", "Орегано", "Перец чили", "Каперсы", "Спагетти", "Соль", "Оливковое масло", "Французский багет ", "Соленые анчоусы в масле"], time: "25 минут", cost: 130, category: "Основные блюда", fullRecipe: """
            Хлеб посушить в духовке при 100 градусах в течение часа. Сломать хлеб руками, чтобы получилась крупная крошка.
        Отварить спагетти в соленой воде в течение 8-9 минут.
        Сделать соус. На оливковом масле обжарить рубленый чеснок, чили, филе анчоуса.
        Когда филе анчоусов начнет растворяться в масле, добавить 10 г хлебной крошки, помидоры в собственном соку, каперсы, соль, перец орегано.
        Оставить соус на 7-8 минут на медленном огне.
        Когда спагетти готовы, выложить в тарелку, вместе с соусом, посыпать орегано и приправить перцем.
        """
        ))
        
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "7") , name: "Сырные кексы с семечками", ingridients: ["Паприка сладкая", "Крупа кукурузная", "Семечки подсолнуха", "Соль", "Яйцо куриное", "Мука пшеничная", "Масло сливочное", "Сыр твердый", "Молоко"], time: "40 минут", cost: 135, category: "Закуски", fullRecipe: """
            Поленту смешать с мукой, паприкой и семечками. Добавить соль по вкусу.
        Сыр ТМ "Белебеевский" (я брала "Сметанковый") натереть на средней терке.
        В другой миске смешать яйца, сливочное масло (комнатной температуры), молоко и сыр.
        Обе массы смешать, замесить негустое тесто. Оставить его на 10-15 минут (чтоб кукурузная крупа разбухла).
        Полученное тесто выложить в мазанные сливочным маслом (или в бумажные) формочки для кексов. Духовку разогреть до 180 гр., запекать 35-40 минут.
        """
        ))
        
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "8") , name: "Ананасовый блюз", ingridients: ["Бальзамик", "Имбирь","Шампиньоны", "Соль", "Капуста пекинская", "Ананас", "Масло подсолнечное", "Апельсин"], time: "10 минут", cost: 120, category: "Закуски", fullRecipe: """
            Для приготовления салата использовалось подсолнечное масло ТМ"ОЛейна"
        Нарезать грибы, пекинскую капусту и апельсин произвольными кусочками
        Почистить и нарезать ананас
        Сешать бальзамик, имбирь, соль и масло ТМ "Олейна". хорошо перемешать
        Заправить салат заправкой, выложить в пиалы, охладить в холодильнике 15 минут и подавать!
        """
        ))
        
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "9") , name: "Шаурма с капустой и курицей", ingridients: ["Масло оливковое", "Капуста нарезанная", "Куриная грудка", "Соль", "Чеснок", "Кетчуп", "Майонез", "Масло для жарки", "Тмин", "Сок лимона", "Помидор", "Карри", "Перец ", "Лаваш", "Огурец"], time: "20 минут", cost: 75, category: "Закуски", fullRecipe: """
            Грудку порежьте на кусочки, сделайте соус из лимонного сока, масло и специй.
        Замаринуйте кусочки курицы на 20 минут.
        Обжарьте курицу на масле для жарки до готовности.
        Капусту, помидор, огурец мелко порежьте.
        Сверху уложите огурцы, майонез и на него выложите помидоры.
        Лаваш поделите на четыре части. В верхней части куска уложите капусту и полейте ее кетчупом.
        Уложите на капусту кусочки курицы.
        Сверху уложите огурцы и майонез.
        Верхний слой закройте помидорами.
        Заверните лаваш рулетиком. Приятного аппетита!
        """
        ))
        
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "10") , name: "Английский кекс (морковный)", ingridients: ["Растительное масло", "Молотая корица", "Пшеничная мука", "Сода", "Сахар", "Молотая гвоздика", "Яйцо куриное", "Толченые грецкие орехи", "Морковь", "Курага", "Соль"], time: "1 час 10 минут", cost: 90, category: "Выпечка", fullRecipe: """
            Смешать сахар, яйца и масло.
        В отдельной посуде смешать муку, соду, корицу, гвоздику и соль.
        Смешать вместе яичную и мучную смеси.
        Добавить орехи, тертую на крупной терке морковь, и курагу. Перемешать до образования пузырьков.
        Вылить в форму и выпекать 50-60 мин при температуре 160 градусов, до темно-коричневого цвета.
        После остывания можно посыпать сахарной пудрой.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "11") , name: "Профитроли с кремом шантильи", ingridients: ["Ванильный стручок", "Пшеничная мука", "Вода", "Сливочное масло", "Сахар", "Молоко", "Яйцо куриное", "Взбитые сливки", "Соль"], time: "30 минут", cost: 120, category: "Выпечка", fullRecipe: """
            Заварное тесто для профитролей: вскипятить воду, добавить 30 грамм сливочного масла, соль, сахар по вкусу, добавить 70 грамм муки и варить еще 10 минут. Снять с огня, затем по очереди вбить 3 яйца с помощью миксера, выложить тесто в виде профитролей на противень и выпекать при 190 градусах 15 минут.
        Крем шантильи: 3 яйца смешать со 150 граммами сахара и 40 граммами муки. Молоко вскипятить, смешать с семенами ванили и налить его в яичную смесь, процедить и варить еще 10 минут. В конце добавить 150 грамм масла. Взбить сливки и вмешать их в ванильный крем.
        Украсить тарелку кремом шантильи и шоколадным соусом, наполнить профитроли кремом, посыпать сахарной пудрой и подать с шоколадным соусом.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "12") , name: "Яблочная шарлотка на скорую руку", ingridients: ["Яблоки", "Кефир", "Мука", "Сода", "Соль", "Сахар", "Яйцо"], time: "45 минут", cost: 60, category: "Выпечка", fullRecipe: """
            1. Яблоки вымыть, очистить от сердцевины и нарезать небольшими кусочками.
        2. Дно формы для запекания смазать небольшим количеством масла или застелить пергаментом. Выложить яблоки.
        3. В небольшой мисочке соединить яйца с сахаром.
        4. Взбить до однородности. Использовать в рецепт приготовления яблочной шарлотки на скорую руку можно щепотку ванилина или корицы для аромата, например.
        5. Влить кефир, добавить щепотку соды или разрыхлитель.
        6. Взбить и постепенно всыпать просеянную муку. Тесто не должно быть очень крутым, тогда яблочная шарлотка на скорую руку в домашних условиях будет мягкой и нежной.
        7. Вылить тесто сверху яблок и отправить в разогретую духовку. Выпекать шарлотку при средней температуре до готовности.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "13") , name: "Диетические овсяные блинчики", ingridients: ["Молоко", "Яйцо куриное", "Вода", "Овсяные хлопья", "Белый мелкий сахар"], time: "25 минут", cost: 40, category: "Десерты", fullRecipe: """
        Соединить молоко и холодную кипяченую воду, добавить хлопья, сварить жидкую овсяную кашу.
        Дать каше остыть до комнатной температуры и перетереть ее вилкой. Добавить соль, сахар, яйцо, хорошо перемешать и выпечь на горячей сковороде тонкие блины.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "14") , name: "Вишневое желе на подушке из чернослива", ingridients: ["Пюре ягодное", "Какао-порошок", "Сухофрукты", "Сахар коричневый", "Агар-агар", "Орехи грецкие"], time: "40 минут", cost: 60, category: "Десерты", fullRecipe: """
            Я использовала замороженную вишню без косточки. Предварительно ее нужно разморозить, слить выделившийся сок в чашку, пюрировать в блендере. Для усиления вкуса можно добавить по желанию 10-15 предварительно заспиртованных вишенок.
        Смесь из вымытых и обсушенных сухофруктов измельчить в блендере с орехами и какао в пюре. Выложить массу в силиконовую форму (у меня размер 9Х22 см), разровнять, утрамбовать рукой, сверху сгладить мокрой силиконовой лопаткой. Отправить в холодильник.
        В вишневое пюре всыпаем 50 г сахара (у меня вишня-черешня была сладкая, поэтому количество сахара регулируем по-вкусу!). Вишневый сок влить к пюре, всыпать 1,5 ч. л. агара, порошок сыплем сверху, НЕ ПЕРЕМЕШИВАЕМ! Нагреваем кастрюлю на среднем огне, за это время агар-агар набухает, образуя на поверхности кашицу. Перемешивать можно, как только жидкость закипит, греем при слабом кипении 2-3 минут, периодически мешая, до полного растворения агар-агара.
        В форму с подготовленной основой вылить остывшее до 60-70 С градусов вишневое пюре, охладить и отправить в холодильник до дальнейшего застывания. Вынуть из формы десерт, нарезать квадратами или прямоугольниками, подавать к столу.
        """
        ))
        
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "15") , name: "Крем-какао с печеньем и орехами по португальски", ingridients: ["Печенье", "Молоко", "Яичный желток", "Орехи", "Какао"], time: "15 минут", cost: 60, category: "Десерты", fullRecipe: """
            Яичные желтки взбейте с молоком и подогрейте, не доводя до кипения. Добавьте какао и перемешайте.
        Измельчите печенье, соедините его с яичной массой и перемешайте. Добавьте измельченные обжаренные орехи. Взбейте получившуюся смесь и залейте в форму для микроволновки.
        Поставьте в микроволновую печь на 6–8 минут. Готовьте при средней мощности.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "16") , name: "Айс Латте Банановая Карамель", ingridients: ["Молоко", "Сироп «Карамель»", "Сироп «Зеленый Банан» ", "Эспрессо доппио"], time: "15 минут", cost: 40, category: "Напитки", fullRecipe: """
            Наполните на 3/4 льдом стакан необходимого объема.
        Добавьте соответствующее количество Бананового и Карамельного сиропов в стакан поверх льда.
        Налейте холодное молоко в стакан поверх льда и сиропа, оставив минимум 2 см от края для необходимого количества порций эспрессо и 1 см пены.
        Взбейте молоко. Добавь 1 см молочной пены в стакан с молоком.
        Приготовьте порцию эспрессо.
        Добавьте соответствующее количество эспрессо поверх молока.
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "17") , name: "Коктейль с манго и ананасом", ingridients: ["Пюре манго", "Листья мяты", "Сок лайма", "Ананасовый ром", "Сахарный сироп", "Лед"], time: "5 минут", cost: 50, category: "Напитки", fullRecipe: """
            1. Разомните мяту с соком лайма и сладким сиропом.
        2. Добавьте ром и пюре манго (просто разомните вилкой или толкушкой манго).
        3. Добавьте в шейкер лед и взболтайте.
        4. В стакан положите пару кубиков льда.
        5. Вылейте готовый коктейль и украсьте по желанию стакан ломтиком ананаса. Подавайте к столу!
        """
        ))
        
        recipies.append(Recipe(image: #imageLiteral(resourceName: "18") , name: "Смузи медово-кефирный со свекольным соком", ingridients: ["Сок", "Мед", "Отруби", "Кефир"], time: "15 минут", cost: 45, category: "Напитки", fullRecipe: """
            Кефир налить в кастрюльку.
        Добавить мед.
        Вареную свеклу натереть на мелкой терке и выдавить сок.
        Перемешать.
        Всыпать отруби и
        взбить миксером.
        Разлить по стаканам.
        Приятного аппетита! Очень вкусно!
        """
        ))
    }
}
